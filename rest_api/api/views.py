from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt

# Create your views here.

import requests

@csrf_exempt
def index(request):
    if request.method == "GET":
        if "username" in request.GET.keys():
            user = User.objects.get(username=request.GET.get("username"))
            if user:
                data = User.objects.filter(username=request.GET.get("username")).values()[0]
                return JsonResponse(data)
        elif "email" in request.GET.keys():
            user = User.objects.get(email=request.GET.get("email"))
            if user:
                data = User.objects.filter(email=request.GET.get("email")).values()[0]
                return JsonResponse(data)
        else:
            return render(request,"index.html")
    else:
        user = User()
        user.username = request.POST.get("username")
        user.email = request.POST.get("email")
        user.set_password(request.POST.get("password"))
        user.save()
        return render(request,"index.html",{"message":"User Created"})